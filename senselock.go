package senselock

/*
#cgo windows,amd64 LDFLAGS: -L${SRCDIR}/libs -lSense_LC_x64
#cgo windows,386 LDFLAGS: -L${SRCDIR}/libs -lSense_LC_x86
#include "libs/Sense_LC.h"
*/
import "C"
import "fmt"
import "unsafe"
import "math/rand"
import "errors"
import "encoding/hex"

type LC_hardware_info struct {
	developerNumber int    // Developer ID
	serialNumber    string // Unique Device Serial Number
	setDate         int    // Manufacturing date
	reservation     int    // Reserve
}

type LC_software_info struct {
	version     int // Software edition
	reservation int // Reserve
}

var handle C.lc_handle_t
var hardware = LC_hardware_info{}
var software = LC_software_info{}
var text *C.char = C.CString(randomSequence(64))

var REMOTE_UPDATE_KEY_TYPE = 0

const (
	AUTH_KEY_TYPE  = 1
	ADMIN_PWD_TYPE = 0
	USER_PWD_TYPE  = 1
	AUTH_PWD_TYPE  = 2
)

var error_message = []string{"EP_SUCCESS", "EP_OPEN_DEVICE_FAILED", "EP_FIND_DEVICE_FAILED", "EP_INVALID_PARAMETER", "EP_INVALID_BLOCK_NUMBER", "EP_HARDWARE_COMMUNICATE_ERROR", "EP_INVALID_PASSWORD", "EP_ACCESS_DENIED", "EP_ALREADY_OPENED", "EP_ALLOCATE_MEMORY_FAILED", "EP_INVALID_UPDATE_PACKAGE", "EP_SYN_ERROR", "EP_OTHER_ERROR"}

func Open(dev_id, device_index int) error {
	var result = C.LC_open(C.int(dev_id), C.int(device_index), &handle)
	if result == 0 {
		return nil
	}
	return errors.New(error_message[result])
}

func VerifyPassword(password_type int, password string) error {
	password_ptr := (*C.uchar)(unsafe.Pointer(C.CString(password)))
	var result = C.LC_passwd(handle, C.int(password_type), password_ptr)
	if result == 0 {
		return nil
	}
	return errors.New(error_message[result])
}

func ReadData(block int) (error, []byte) {
	buf := make([]byte, 512)
	buf_ptr := (*C.uchar)(unsafe.Pointer(&buf[0]))
	var result = C.LC_read(handle, C.int(block), buf_ptr)
	//fmt.Printf("%s\n", buf[64:])
	if result == 0 {
		return nil, buf
	}
	return errors.New(error_message[result]), buf
}

func getHardwareInfo() {
	hardware_ptr := (*C.LC_hardware_info)(unsafe.Pointer(&hardware))
	C.LC_get_hardware_info(handle, hardware_ptr)
	fmt.Println(*hardware_ptr)
}

func getSoftwareInfo() {
	software_ptr := (*C.LC_software_info)(unsafe.Pointer(&software))
	C.LC_get_software_info(software_ptr)
	fmt.Println(*software_ptr)
}

func randomSequence(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func getLocalDigest(local_digest_interval int) []byte {
	hmac_key := make([]byte, 20)
	for i := 0; i < 20; i++ {
		hmac_key[i] = byte(0x0A + (i * local_digest_interval))
	}

	text_ptr := (*C.uchar)(unsafe.Pointer(text))
	digest := make([]byte, 20)
	digest_ptr := (*C.uchar)(unsafe.Pointer(&digest[0]))

	hmac_key_ptr := (*C.uchar)(unsafe.Pointer(&hmac_key[0]))
	C.LC_hmac_software(text_ptr, 56, hmac_key_ptr, digest_ptr)
	//fmt.Printf("% 02x\n", digest)
	return digest
}

func getDeviceDigest() []byte {
	text_ptr := (*C.uchar)(unsafe.Pointer(text))
	digest := make([]byte, 20)
	digest_ptr := (*C.uchar)(unsafe.Pointer(&digest[0]))
	C.LC_hmac(handle, text_ptr, 56, digest_ptr)
	//fmt.Printf("% 02x\n", digest)
	return digest
}

func WriteData(block int, data []byte) error {
	data_ptr := (*C.uchar)(unsafe.Pointer(&data[0]))
	result := C.LC_write(handle, C.int(block), data_ptr)
	if result == 0 {
		return nil
	}
	return errors.New(error_message[result])
}

func setPassword(password_type int, password string, retries int) error {
	data := make([]byte, 8)
	for i, v := range []byte(password) {
		data[i] = byte(v)
	}
	password_ptr := (*C.uchar)(unsafe.Pointer(&data[0]))
	result := C.LC_set_passwd(handle, C.int(password_type), password_ptr, C.int(retries))
	if result == 0 {
		return nil
	}
	return errors.New(error_message[result])
}

func changePassword(password_type int, old_password, new_password string) error {
	old_password_data := make([]byte, 8)
	for i, v := range []byte(old_password) {
		old_password_data[i] = byte(v)
	}
	old_password_ptr := (*C.uchar)(unsafe.Pointer(&old_password_data[0]))

	new_password_data := make([]byte, 8)
	for i, v := range []byte(new_password) {
		new_password_data[i] = byte(v)
	}
	new_password_ptr := (*C.uchar)(unsafe.Pointer(&new_password_data[0]))

	result := C.LC_change_passwd(handle, C.int(password_type), old_password_ptr, new_password_ptr)
	if result == 0 {
		return nil
	}
	return errors.New(error_message[result])
}

func SetKey(key_type int, key string) error {
	decoded_key, err := hex.DecodeString(key)
	if err != nil {
		return err
	} else {
		//fmt.Println(string(decoded_key))
		key_data_ptr := (*C.uchar)(unsafe.Pointer(&decoded_key[0]))
		result := C.LC_set_key(handle, C.int(key_type), key_data_ptr)
		if result == 0 {
			return nil
		}
		return errors.New(error_message[result])
	}
}

func Close() error {
	result := C.LC_close(handle)
	if result == 0 {
		return nil
	}
	return errors.New(error_message[result])
}

func CompareHmac(local_digest_interval int) error {
	device_digest := getDeviceDigest()
	local_digest := getLocalDigest(local_digest_interval)
	for i := 0; i < 20; i++ {
		if device_digest[i] != local_digest[i] {
			return errors.New("EP_DEGESTS_NOT_MATCHED")
		}
	}
	return nil
}

func SetPassword(dev_id, device_index int, admin_password string, password_type int, new_password string, retries int) error {
	err := Open(dev_id, device_index)
	if err != nil {
		return err
	} else {
		err := VerifyPassword(ADMIN_PWD_TYPE, admin_password)
		if err != nil {
			return err
		} else {
			err := setPassword(password_type, new_password, retries)
			if err != nil {
				return err
			} else {
				err := Close()
				return err
			}
		}
	}
}

func ChangePassword(dev_id, device_index int, password_type int, old_password, new_password string) error {
	err := Open(dev_id, device_index)
	if err != nil {
		return err
	} else {
		err := changePassword(password_type, old_password, new_password)
		if err != nil {
			return err
		} else {
			err := Close()
			return err
		}
	}
}

func SetAuthenticationKey(dev_id, device_index int, admin_password string, key string) error {
	err := Open(dev_id, device_index)
	if err != nil {
		return err
	} else {
		err := VerifyPassword(ADMIN_PWD_TYPE, admin_password)
		if err != nil {
			return err
		} else {
			err := SetKey(AUTH_KEY_TYPE, key)
			if err != nil {
				return err
			} else {
				err := Close()
				return err
			}
		}
	}
}

/*
func Update(data string) error {
  data_str := make([]byte, 256)
  for v, i := range []byte(data) {
    data_str[i] = byte(v)
  }
  data_str_ptr := (*C.uchar)(unsafe.Pointer(&data_str[0]))
  result := C.LC_update(handle, data_str_ptr)
  if result == 0 {
    return nil
  }
  return errors.New(error_message[result])
}

func GenerateUpdatePackage(dongle_serial_number string, block int, update_content, remote_update_key string)(error, []byte) {
  dongle_serial_number_data := make([]byte, 20)
  for v, i := range []byte(dongle_serial_number) {
    dongle_serial_number_data[i] = byte(v)
  }
  dongle_serial_number_ptr := (*C.uchar)(unsafe.Pointer(&dongle_serial_number_data[0]))

  update_content_data := make([]byte, 512)
  for v, i := range []byte(update_content) {
    update_content_data[i] = byte(v)
  }
  update_content_ptr := (*C.uchar)(unsafe.Pointer(&update_content_data[0]))

  remote_update_key_data := make([]byte, 20)
  for v, i := range []byte(remote_update_key) {
    remote_update_key_data[i] = byte(v)
  }
  remote_update_key_ptr := (*C.uchar)(unsafe.Pointer(&remote_update_key_data[0]))

  update_package := make([]byte, 20)
  update_package_ptr := (*C.uchar)(unsafe.Pointer(&update_package[0]))

  result := C.LC_gen_update_pkg(dongle_serial_number_ptr, C.int(block), update_content_ptr, remote_update_key_ptr, update_package_ptr)

  if result == 0 {
    return nil, update_package
  }
  return errors.New(error_message[result]), update_package
}
*/
